From 3f58194f8642a71c47d91d3c00a34faf44ea2c11 Mon Sep 17 00:00:00 2001
From: Eric Auger <eric.auger@redhat.com>
Date: Wed, 3 Jan 2024 05:57:38 -0500
Subject: [PATCH] hw/arm/virt: Fix compats

RH-Author: Eric Auger <eric.auger@redhat.com>
RH-MergeRequest: 209: hw/arm/virt: Fix compats
RH-Jira: RHEL-17168
RH-Acked-by: Gavin Shan <gshan@redhat.com>
RH-Acked-by: Sebastian Ott <sebott@redhat.com>
RH-Commit: [1/1] bcdf6493bbd6d7b52b0b88ff44441d22aeddfde2 (eauger1/centos-qemu-kvm)

arm_rhel_compat is not added for virt-rhel9.4.0 machine causing
the efi-virtio.rom to be looked for when instantiating a virtio-net-pci
device and it won't be found since not shipped on ARM. This is a
regression compared to 9.2.

Actually we do not need any rom file for any virtio-net-pci variant
because edk2 already brings the functionality. So for 9.4 onwards, we
want to set romfiles to "" for all of them.

However at the moment we apply arm_rhel_compat from the latest
rhel*_virt_options(). This is not aligned with the generic compat
usage which sets compats for a given machine type to accomodate for
changes that occured after its advent. Here we are somehow abusing
the compat infra to set general driver options that should apply for
all machines.  On top of that this is really error prone and we have
forgotten to add arm_rhel_compat several times in the past.

So let's introduce set_arm_rhel_compat() being called before any
*virt_options in the non abstract machine class. That way the setting
will apply to any machine type without any need to add it in any
future machine types.

For < 9.4 machines we don't really care keeping non void romfiles
for transitional and non transitional devices because anyway this was
not working. So let's keep things simple and apply the new defaults for
all RHEL9 machine types.

Finally, to follow the generic pattern we should set hw_compat_rhel_9_0
in 9.0 machine as it is done on x86 or ccw. This has no consequence on
aarch64 because it only contains x86 stuff but that helps understanding
the consistency.

Signed-off-by: Eric Auger <eric.auger@redhat.com>
---
 hw/arm/virt.c | 43 +++++++++++++++++++++++++++++--------------
 1 file changed, 29 insertions(+), 14 deletions(-)

diff --git a/hw/arm/virt.c b/hw/arm/virt.c
index 0b17c94ad7..5cab00b4cd 100644
--- a/hw/arm/virt.c
+++ b/hw/arm/virt.c
@@ -111,11 +111,39 @@
     DEFINE_VIRT_MACHINE_LATEST(major, minor, false)
 #endif /* disabled for RHEL */
 
+/*
+ * This variable is for changes to properties that are RHEL specific,
+ * different to the current upstream and to be applied to the latest
+ * machine type. They may be overriden by older machine compats.
+ *
+ * virtio-net-pci variant romfiles are not needed because edk2 does
+ * fully support the pxe boot. Besides virtio romfiles are not shipped
+ * on rhel/aarch64.
+ */
+GlobalProperty arm_rhel_compat[] = {
+    {"virtio-net-pci", "romfile", "" },
+    {"virtio-net-pci-transitional", "romfile", "" },
+    {"virtio-net-pci-non-transitional", "romfile", "" },
+};
+const size_t arm_rhel_compat_len = G_N_ELEMENTS(arm_rhel_compat);
+
+/*
+ * This cannot be called from the rhel_virt_class_init() because
+ * TYPE_RHEL_MACHINE is abstract and mc->compat_props g_ptr_array_new()
+ * only is called on virt-rhelm.n.s non abstract class init.
+ */
+static void arm_rhel_compat_set(MachineClass *mc)
+{
+    compat_props_add(mc->compat_props, arm_rhel_compat,
+                     arm_rhel_compat_len);
+}
+
 #define DEFINE_RHEL_MACHINE_LATEST(m, n, s, latest)                     \
     static void rhel##m##n##s##_virt_class_init(ObjectClass *oc,        \
                                                 void *data)             \
     {                                                                   \
         MachineClass *mc = MACHINE_CLASS(oc);                           \
+        arm_rhel_compat_set(mc);                                        \
         rhel##m##n##s##_virt_options(mc);                               \
         mc->desc = "RHEL " # m "." # n "." # s " ARM Virtual Machine";  \
         if (latest) {                                                   \
@@ -139,19 +167,6 @@
 #define DEFINE_RHEL_MACHINE(major, minor, subminor)             \
     DEFINE_RHEL_MACHINE_LATEST(major, minor, subminor, false)
 
-/* This variable is for changes to properties that are RHEL specific,
- * different to the current upstream and to be applied to the latest
- * machine type.
- */
-GlobalProperty arm_rhel_compat[] = {
-    {
-        .driver   = "virtio-net-pci",
-        .property = "romfile",
-        .value    = "",
-    },
-};
-const size_t arm_rhel_compat_len = G_N_ELEMENTS(arm_rhel_compat);
-
 /* Number of external interrupt lines to configure the GIC with */
 #define NUM_IRQS 256
 
@@ -3639,7 +3654,6 @@ static void rhel920_virt_options(MachineClass *mc)
 {
     rhel940_virt_options(mc);
 
-    compat_props_add(mc->compat_props, arm_rhel_compat, arm_rhel_compat_len);
     compat_props_add(mc->compat_props, hw_compat_rhel_9_4, hw_compat_rhel_9_4_len);
     compat_props_add(mc->compat_props, hw_compat_rhel_9_3, hw_compat_rhel_9_3_len);
     compat_props_add(mc->compat_props, hw_compat_rhel_9_2, hw_compat_rhel_9_2_len);
@@ -3653,6 +3667,7 @@ static void rhel900_virt_options(MachineClass *mc)
     rhel920_virt_options(mc);
 
     compat_props_add(mc->compat_props, hw_compat_rhel_9_1, hw_compat_rhel_9_1_len);
+    compat_props_add(mc->compat_props, hw_compat_rhel_9_0, hw_compat_rhel_9_0_len);
 
     /* Disable FEAT_LPA2 since old kernels (<= v5.12) don't boot with that feature */
     vmc->no_tcg_lpa2 = true;
-- 
2.39.3

